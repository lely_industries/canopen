---
title: "Installation"
permalink: /docs/installation/
toc: true
---

[Linux](#linux) is the recommended platform for the Lely CANopen stack. While it
is possible to build the stack on [Windows](#windows) with
[Visual Studio](https://visualstudio.microsoft.com/) (not documented here) or
[Cygwin](#cygwin), it is generally easier to cross-compile the stack on Linux
with [MinGW]({{ '/docs/cross-compilation/#mingw-on-linux' | relative_url }}).
Instructions on how to cross-compile the stack for other (embedded) platforms
can be found [here]({{ '/docs/cross-compilation/' | relative_url }}).

## Linux

Binary packages are available for the latest release. If you want to use an
older release, or the current `master` branch, you need to
[build the stack from source](#building-from-source).

When developing and debugging CAN(open) applications on Linux, the `candump` and
`cansend` tools from [can-utils](https://github.com/linux-can/can-utils) will
come in handy. They are available in the standard repositories of most Linux
distributions.
{: .notice--info}

### Binary packages

#### Debian/Ubuntu

For Debian/Ubuntu-based distributions, binary packages can be obtained from the
[Lely PPA](https://launchpad.net/~lely/+archive/ubuntu/ppa). Add the PPA to your
system with
```sh
sudo add-apt-repository ppa:lely/ppa
sudo apt-get update
```

Besides the CANopen stack, this PPA also contains the
[faketime](https://gitlab.com/lely_industries/faketime) tools. These tools allow
a user to run a command in an environment in which all or some of the
system-wide real time clocks run at a user-specified rate and with a
user-specified offset. This can be particularly useful to speed up tests without
having to change the timing parameters.
{: .notice--info}

To install the CANopen development packages and tools, run
```sh
sudo apt-get install liblely-coapp-dev liblely-co-tools python3-dcf-tools
```

Additionally, you may want to install
[`pkg-config`](https://www.freedesktop.org/wiki/Software/pkg-config/):
```sh
sudo apt-get install pkg-config
```
`pkg-config` allows you to obtain the compiler and linker flags needed to use
one of the core libraries. For example:
```sh
pkg-config --cflags liblely-coapp
pkg-config --libs liblely-coapp
```
Most build systems have built-in support for `pkg-config`.

Finally, the [can-utils](https://github.com/linux-can/can-utils) package
contains several tools, such as `candump` and `cansend`, that may prove useful
for testing and debugging. It can be installed with
```sh
sudo apt-get install can-utils
```

### Building from source

#### Requirements

- [Git](https://git-scm.com/) (check by running `git --version`);
- [GCC](https://gcc.gnu.org/) (check by running `gcc -v` and `g++ -v`) or [Clang](https://clang.llvm.org/) (check by running `clang -v` and `clang++ -v`);
- [Make](https://www.gnu.org/software/make/) (check by running `make -v`);
- GNU Autotools: [autoconf](https://www.gnu.org/software/autoconf/), [automake](https://www.gnu.org/software/automake/) and [libtool](https://www.gnu.org/software/libtool/) (check by running `autoreconf -V` and `libtoolize --version`).

Git is not necessary if you download the source code of one of the
[releases](https://gitlab.com/lely_industries/lely-core/-/releases).
{: .notice--info}

To build the [DCF tools]({{ '/docs/dcf-tools/' | relative_url }}), you need
[Python 3](https://www.python.org/) with the
[setuptools](https://pypi.org/project/setuptools/) and
[wheel](https://pypi.org/project/wheel/) modules. To run the tools, you also
need the [EmPy](https://pypi.org/project/empy/) and
[PyYAML](https://pypi.org/project/PyYAML/) modules.

The optional Python bindings for the (deprecated) CAN and I/O libraries require
[Cython](https://cython.org/).

Under Linux, Bluetooth support in the I/O library requires the
[BlueZ](http://www.bluez.org/) headers (the library is not used). It is not
necessary for the CANopen stack, so it can be omitted.

If you plan on using [Valgrind](http://valgrind.org/), make sure it is installed
when the stack is built. Otherwise Valgrind may complain about stack changes in
code using [fibers](https://en.wikipedia.org/wiki/Fiber_%28computer_science%29).

The `configure` script will look for the `<valgrind/valgrind.h>` header and, if
it is present, insert calls to `VALGRIND_STACK_REGISTER()` and
`VALGRIND_STACK_DEREGISTER()` in the fiber code to notify Valgrind about stack
changes.
{: .notice--info}

Finally, if you want to generate the HTML documentation of the API, make sure
[doxygen](http://www.doxygen.nl/) and [Graphviz](https://www.graphviz.org/) are
installed.

On Debian/Ubuntu-based distributions, all dependencies (except Cython) can be
installed with
```sh
sudo apt-get install \
    git build-essential automake libtool \
    python3-setuptools python3-wheel \
    python3-empy python3-yaml \
    libbluetooth-dev \
    valgrind \
    doxygen graphviz
```
If you want to use Clang instead of GCC, install it with
```sh
sudo apt-get install clang
```

#### Build instructions

Clone the repository with
```sh
git clone https://gitlab.com/lely_industries/lely-core.git
cd lely-core
```

Initialize the build system (to create the `configure` script):
```sh
autoreconf -i
```
and build environment:
```sh
mkdir -p build
cd build
../configure --disable-cython
```
`--disable-cython` disables the deprecated Python bindings.
{: .notice--info}

A description of all the configuration options can be found
[here]({{ '/docs/configuration/' | relative_url }}).

If you want to use Clang, prefix the `configure` command with `CC=clang` and
`CXX=clang++`.
{: .notice--info}

Run
```sh
make
```
to build the stack. If you want, you can run the test suite with
```sh
make check
```
This takes about a minute.

Run `make check-valgrind-memcheck` to run the entire test suite under valgrind.
This takes considerably longer.
{: .notice--info}

Finally, run
```sh
make install
```
to install the headers, libraries, tools and, if you installed doxygen, the
HTML documentation.

By default, `make install` will install everything under `/usr/local`. You can
change this directory by specifying the `--prefix` option to `configure`.
{: .notice--info}

## Windows

Only the Cygwin installation process is described here. While it is possible to
build and use the stack with Visual Studio, we have not yet documented the
process.

Click [here]({{ '/docs/cross-compilation/#mingw-on-linux' | relative_url }}) for
instructions on how to cross-compile the stack with MinGW on Linux.

### Cygwin

Download the [Cygwin](https://www.cygwin.com/) setup program. Both the 32-bit
and 64-bit version van be used.

Run `setup-x86.exe` or (`setup-x86_64.exe`) and select the `git`, `gcc-g++`,
`make`, `automake` and `libtool` packages.

Open a Cygwin terminal and follow the [build instructions](#build-instructions).
