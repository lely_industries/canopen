---
title: "New release: v2.1.4 and v2.0.5"
author: jseldent
categories: release
---

v2.1.4 and v2.0.5 have just been released. These are minor releases containing
only bugfixes.

### Bug fixes

The following bugs have been fixed:
- Some of the functions in `<lely/util/bits.h>` would truncate 64-bit values on
  MinGW.
- Specifying the `--disable-canfd` configure option (or `LELY_NO_CANFD` macro)
  would cause unused-variable warnings and therefore build failures.
- During the LSS configure bit timing parameters request, a requested bit rate
  of 125 kbit/s was interpreted as 135 kbit/s.
- Including `<inttypes.h>` requires `<sys/types.h>` when using Newlib.
- Submitting a wait operation to a timer queue could cause uninitialized
  variable access.

### Download

You can download the source from
[GitLab](https://gitlab.com/lely_industries/lely-core/-/archive/v2.1.4/lely-core-v2.1.4.zip)
or the Ubuntu packages from our
[PPA](https://launchpad.net/~lely/+archive/ubuntu/ppa).
