---
title: "New release: v2.0.2"
author: jseldent
categories: release
---

v2.0.2 has just been released. This is a minor release containing only bugfixes
and cleanups. No new functionality has been added, but some functionality has
been made optional to reduce RAM and ROM requirements on resource-constrained
platforms. And no behavior has changed, except for platforms where
`CHAR_BIT != 8`.

### Reduced RAM and ROM requirements

Two new configure/preprocessor options have been added to reduce the runtime
size of the object dictionary:

* `--disable-obj-default` (`LELY_NO_CO_OBJ_DEFAULT`):
  disable default values in the object dictionary.
* `--disable-obj-upload` (`LELY_NO_CO_OBJ_UPLOAD`):
  disable custom upload indication functions in the object dictionary.

Together with the existing options to disable names and minimum/maximum values,
these options are no also taken into account when generating the static device
description. This can significantly reduce the ROM size.

CiA 301 requires that upon receipt of the NMT "Reset application" command, the
parameters of the manufacturer-specific profile area (object 2000...5FFF) and of
the standardized device profile area (object 6000..9FFF) are set to their
power-on values. The stack implements this requirement by storing the values of
those objects as a concise DCF in memory on initialization, and restoring them
when the NMT command is received.

However, embedded devices usually reboot in response to a "Reset application"
command, which already resets the object dictionary. In that case, storing the
concise DCF is not necessary and can be disabled with the
`--disable-dcf-restore` configure option or the `LELY_NO_CO_DCF_RESTORE`
preprocessor macro.

### `CHAR_BIT != 8`

Nearly all computer architectures provide 8-bit bytes. However, one of the
platforms we would like to support, the C28x Piccolo DSP (part of the
[C2000](https://en.wikipedia.org/wiki/Texas_Instruments_TMS320#C2000_series)
microcontroller family of Texas Instruments), has 16-bit bytes. This is now
supported by the CANopen stack and supporting libraries.

Byte arrays, such as CAN frame payloads and serialized values, are treated as
sequences of 8-bit bytes, even if `CHAR_BIT != 8`. The most significat bits are
discarded, if necessary. This wastes some memory on platforms like the C28x
Piccolo DSP, but it keeps the code and type definitions simpler and more
maintainable. To prevent mistakes, this choice has been made explicit in the
function prototypes of the byte-order functions in `<lely/util/endian.h>`.

v2.0.2 still assumes the availability of the `uint8_t` typedef. This has already
been rectified in the master branch, and from v2.1 onward, the stack will no
longer assume the availability of any exact-width integer type. However, this
fix touches too much code to be backported to v2.0.
{: .notice--info}

### Download

You can download the source from
[GitLab](https://gitlab.com/lely_industries/lely-core/-/archive/v2.0.2/lely-core-v2.0.2.zip)
or the Ubuntu packages from our
[PPA](https://launchpad.net/~lely/+archive/ubuntu/ppa).
